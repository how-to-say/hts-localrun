# hts-localrun

Run locally using docker-compose with addional logic to replace docker-images with local ones (specially for tests).

# Config

Configurations will be setted to default values if none if specify. To change this values, copy the skel config and edit the new file.

```bash
cp config/how-to-say.cfg.skel config/how-to-say.cfg
```

Run `make` to set up all configurations needed.

# Running

To run an instance with the most updated images, run `make run`
