HTS_FINAL_DIR=${HOME}"/.how-to-say-runner"
LOCAL_DIR_DIRECTION_FILE=".how-to-say-localrun-dir"
all: set

set:
	echo "Setting up"
	test -e config/how-to-say.cfg || cp config/how-to-say.cfg.skel config/how-to-say.cfg
	rm -rf $(HTS_FINAL_DIR)
	mkdir -p $(HTS_FINAL_DIR)
	cp -r * $(HTS_FINAL_DIR)
	pwd > $(HTS_FINAL_DIR)/$(LOCAL_DIR_DIRECTION_FILE)

run: set
	make -C $(HTS_FINAL_DIR) _down _up

_down:
	echo "Cleaning"
	docker-compose down

_up:
	echo "Running"
	docker-compose up --build
	_down

reset:
	if [ ! -f $(LOCAL_DIR_DIRECTION_FILE) ]; then \
		exit 1; \
	fi
	make -C $(shell cat $(LOCAL_DIR_DIRECTION_FILE)) set

.PHONY: set up down reset
